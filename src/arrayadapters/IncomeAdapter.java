package arrayadapters;

import java.util.ArrayList;
import java.util.Date;

import objects.Income;
import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * An adapter that shows Income-objects in a listview.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class IncomeAdapter extends ArrayAdapter<Income> {
	private Context context;
	private int layoutResourceId;
	private ArrayList<Income> data = null;

	public IncomeAdapter(Context context, int resource, ArrayList<Income> data) {
		super(context, resource, data);
		this.layoutResourceId = resource;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_income, parent, false);

		TextView tv_title = (TextView) rowView
				.findViewById(R.id.row_income_title);
		TextView tv_category = (TextView) rowView
				.findViewById(R.id.row_income_category);
		TextView tv_price = (TextView) rowView
				.findViewById(R.id.row_income_price);
		TextView tv_date = (TextView) rowView
				.findViewById(R.id.row_income_date);
		Date timestamp = new Date(data.get(position).getTimestamp());

		tv_title.setText(data.get(position).getTitle());
		tv_category.setText(data.get(position).getCategory());
		tv_price.setText(data.get(position).getAmount().toString()+UserHandler.getStaticCurrency());
		tv_date.setText(timestamp.toString());
		return rowView;
	}
}
