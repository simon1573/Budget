package arrayadapters;

import java.util.ArrayList;
import java.util.Date;

import objects.Expense;
import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An adapter that shows Expense-objects in a listview.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class ExpenseAdapter extends ArrayAdapter<Expense>{
	private Context context;
	private int layoutResourceId;
	private ArrayList<Expense> data = null;

	public ExpenseAdapter(Context context, int resource, ArrayList<Expense> data) {
		super(context, resource, data);
		this.layoutResourceId = resource;
		this.context = context;
		this.data = data;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_expense, parent, false);
		
		TextView tv_title = (TextView)rowView.findViewById(R.id.row_expense_title);
		TextView tv_category = (TextView)rowView.findViewById(R.id.row_expense_category);
		TextView tv_price = (TextView)rowView.findViewById(R.id.row_expense_price);
		TextView tv_date = (TextView)rowView.findViewById(R.id.row_expense_date);
		ImageView image = (ImageView)rowView.findViewById(R.id.row_expense_thumbnail);
		Date timestamp = new Date(data.get(position).getTimestamp());
		
		tv_title.setText(data.get(position).getTitle());
		tv_category.setText(data.get(position).getCategory());
		tv_price.setText(data.get(position).getAmount().toString()+UserHandler.getStaticCurrency());
		tv_date.setText(timestamp.toString());
		if (image != null) {
			image.setImageBitmap(data.get(position).getImage());
		}
		return rowView;
	}
}
