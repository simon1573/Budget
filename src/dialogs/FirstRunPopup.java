package dialogs;

import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Creates a dialog where users are forced to submit thier full name and prefered currency.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class FirstRunPopup extends DialogFragment {

	private EditText et_name, et_surname;
	private Dialog dialogview;
	private UserHandler controller;
	private String firstname, surname, currency;
	private Spinner spinner_currency;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		dialogview = getDialog();
		controller = new UserHandler();

		builder.setView(inflater.inflate(R.layout.dialog_firstrun, null))
				.setPositiveButton(getString(R.string.submit),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Overridden to force user to submit name
							}
						});

		setCancelable(false);
		AlertDialog dialog = builder.create();
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						dialogview = getDialog();
						et_name = (EditText) dialogview.findViewById(R.id.et_firstrun_firstname);
						et_surname = (EditText) dialogview.findViewById(R.id.et_firstrun_surname);
						spinner_currency = (Spinner)dialogview.findViewById(R.id.spinner_currency);
						firstname = et_name.getText().toString();
						surname = et_surname.getText().toString();
						currency = spinner_currency.getSelectedItem().toString();
						if (firstname.length() > 0 && surname.length() > 0) {
							 controller.saveUser(firstname, surname, currency);
							dismiss();
							Toast.makeText(dialogview.getOwnerActivity(), getString(R.string.welcome) + " " + firstname + " "+surname, Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(dialogview.getOwnerActivity(), R.string.tutorial_submit_name, Toast.LENGTH_SHORT).show();
						}
					}
				});
		return dialog;
	}
}