package main_fragments;

import java.sql.Date;
import java.util.ArrayList;

import objects.Expense;
import objects.Income;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import others.ContextPasser;
import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import controllers.SummaryController;

/**
 * A fragment that lets the user see statistics of their economy.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class SummaryFragment extends Fragment {
	private Context context = ContextPasser.getLocalContext();
	private SummaryController controller = new SummaryController();
	private UserHandler userHander = new UserHandler();
	private static String currencySuffix;
	private View view = null;
	private TextView tv_totalExpense ,tv_totalIncome, tv_totalSum,tv_title_totalSum,tv_personal_bankruptcy_bar_title;
	private ProgressBar bar;
	private TableRow sumRow;

	// Diagram
	private GraphicalView incomeDiagramView;
	private GraphicalView expenseDiagramView;
	private XYMultipleSeriesDataset incomeDataset = new XYMultipleSeriesDataset();
	private XYMultipleSeriesDataset expenseDataset = new XYMultipleSeriesDataset();
	private XYMultipleSeriesRenderer incomeRenderer = new XYMultipleSeriesRenderer();
	private XYMultipleSeriesRenderer expenseRenderer = new XYMultipleSeriesRenderer();
	private TimeSeries currentIncomeSeries;
	private TimeSeries currentExpenseSeries;
	private XYSeriesRenderer currentIncomeRenderer;
	private XYSeriesRenderer currentExpenseRenderer;
	// -------


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i("Budget", "Fragment summary_fragment initiated!");
		view = inflater.inflate(R.layout.frag_summary, null);
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		currencySuffix = UserHandler.getStaticCurrency();
		initComponents();
		setComponents();
		setHasOptionsMenu(true);
			
			
		// Diagram
		initDiagrams();
		loadIncomeData();
		loadExpenseData();
		setDiagrams();
		// --------
	};
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
	}

	/**
	 * Initilizes the components.
	 */
	private void initComponents() {
		 tv_totalExpense = (TextView) view.findViewById(R.id.tv_summay_row_expense);
		 tv_totalIncome = (TextView) view.findViewById(R.id.tv_summay_row_income);
		 tv_totalSum = (TextView) view.findViewById(R.id.tv_summay_row_combined);
		 tv_title_totalSum = (TextView) view.findViewById(R.id.tv_summary_title_sum);		
		 tv_personal_bankruptcy_bar_title = (TextView) view.findViewById(R.id.tv_summary_personal_bankruptcy_bar_title);
		 bar = (ProgressBar) view.findViewById(R.id.progressbar_summary);
		 sumRow = (TableRow) view.findViewById(R.id.tablerow_summary_sum);
	}

	/**
	 * Sets values to the components.
	 */
	private void setComponents() {
		currencySuffix = UserHandler.getStaticCurrency();
		long totalExpense = controller.getTotalExpense();
		long totalIncome = controller.getTotalIncome();
		long totalSum = totalIncome - totalExpense;
		tv_totalExpense.setText(String.valueOf(totalExpense) + currencySuffix);
		tv_totalIncome.setText(String.valueOf(totalIncome) + currencySuffix);
		tv_totalSum.setText(String.valueOf(totalSum) + currencySuffix);
		tv_personal_bankruptcy_bar_title.setText(getText(R.string.personal_bankruptcy_bar_title)+" "+userHander.getFullName());
		if (totalSum >= 0) {
			tv_title_totalSum.setText(getString(R.string.surplus));
			sumRow.setBackgroundColor(0xFF7AE969);
		}

		if (totalExpense >= totalIncome) {
			bar.setProgress(bar.getMax());
		}else {
			bar.setMax((int) (totalIncome / 10));
			bar.setProgress((int) (totalExpense / 10));
		}
	}
	
	/**
	 * Initilizes the diagrams.
	 */
	private void initDiagrams() {
		currentIncomeSeries = new TimeSeries("Income");
		currentExpenseSeries = new TimeSeries("Expense");
		incomeDataset.addSeries(currentIncomeSeries);
		expenseDataset.addSeries(currentExpenseSeries);
		currentIncomeRenderer = new XYSeriesRenderer();
		currentExpenseRenderer = new XYSeriesRenderer();
		

		currentIncomeSeries.setTitle(getString(R.string.incomes));
		currentIncomeRenderer.setColor(Color.RED);

		currentIncomeRenderer.setPointStyle(PointStyle.CIRCLE);
		currentIncomeRenderer.setFillPoints(true);
		currentIncomeRenderer.setDisplayChartValues(true);
		currentIncomeRenderer.setDisplayChartValuesDistance(10);


		currentExpenseSeries.setTitle(getString(R.string.expenses));
		currentExpenseRenderer.setColor(Color.RED);
		
		currentExpenseRenderer.setPointStyle(PointStyle.CIRCLE);
		currentExpenseRenderer.setFillPoints(true);
		currentExpenseRenderer.setDisplayChartValues(true);
		currentExpenseRenderer.setDisplayChartValuesDistance(10);
		
		incomeRenderer.addSeriesRenderer(currentIncomeRenderer);
		expenseRenderer.addSeriesRenderer(currentExpenseRenderer);
	}

	/**
	 * Sets values to the diagrams.
	 */
	private void setDiagrams() {
		incomeDiagramView = ChartFactory.getTimeChartView(context, incomeDataset,
				incomeRenderer,"yyyy-MM-dd");
		expenseDiagramView = ChartFactory.getTimeChartView(context, expenseDataset,
				expenseRenderer,"yyyy-MM-dd");
		LinearLayout incomeDiagram = (LinearLayout) view
				.findViewById(R.id.view_summary_income_diagramview);
		LinearLayout expenseDiagram = (LinearLayout) view
				.findViewById(R.id.view_summary_expense_diagramview);
		incomeDiagram.addView(incomeDiagramView);
		expenseDiagram.addView(expenseDiagramView);
	}

	/**
	 * Gets all incomes and adds it to the diagram data.
	 */
	private void loadIncomeData() {
		ArrayList<Income> incomes = controller.getIncome();
		for (int i = 0; i < incomes.size(); i++) {
			long date = incomes.get(i).getTimestamp();
			double y = incomes.get(i).getAmount();
			Date x = new Date(date);
			currentIncomeSeries.add(x, y);
		}
	}
	
	/**
	 * Gets all expenses and adds it to the diagram data.
	 */
	private void loadExpenseData() {
		ArrayList<Expense> expenses = controller.getExpense();
		for (int i = 0; i < expenses.size(); i++) {
			long date = expenses.get(i).getTimestamp();
			double y = expenses.get(i).getAmount();
			Date x = new Date(date);
			currentExpenseSeries.add(x, y);
		}
	}
}